<!---
Please read this!

Before opening a new change request, make sure to search for keywords in the issues filtered by the "change request" or "type::enhancement" label:

- https://gitlab.com/gadhs/gateway/issues?label_name%5B%5D=change%20request
- https://gitlab.com/gadhs/gateway/gitlab/issues?label_name%5B%5D=type::enhancement

and verify the change request you're about to submit isn't a duplicate.
--->

### Title of Change Request

<!-- Brief, high level description of the proposed change -->

### Business Owner

<!-- Name of the primary program stakeholder -->

### Program

<!-- This is a required field -->

- [ ] Medicaid
- [ ] SNAP
- [ ] TANF
- [ ] PCK
- [ ] CAPS
- [ ] WIC
- [ ] Refugee
- [ ] All Programs

### Area

<!-- This is an optional field -->

- [ ] Reporting
- [ ] Technical


### Reason

<!-- This is an optional field -->

- [ ] Legal
- [ ] State Mandate
- [ ] Federal Mandate
- [ ] Policy Change

### Projects

<!-- This is a required field -->

- [ ] Maintenance & Operations (M&O)
- [ ] Cloud *Amendment does NOT include enhancement hours*
- [ ] Waiver *Amendment does NOT include enhancement hours*

### Design Documents

- [ ] BRS
- [ ] DSD
- [ ] Story Board
- [ ] JAD Sessions
- [ ] Meeting minutes

### Request Type

- [ ] M&O
- [ ] Enhancement
- [ ] M&O – WUAS Update (Update Office Address, Create New Office, Reference Table Update)

### Detailed description

<!--  Detailed description and purpose of the proposed change -->

### Ease of Use Benefit

<!-- Explain how this change will improve the ease of use -->

### Workaround

<!--  Detailed description of the workarounds currently in place.
Include estimated excess employee hour spend required to maintain workaround
-->

### Justification

<!-- Elaborate as to why the change is needed and the potential impact if the change is not processed.
Details must include if this is new policy, missed requirement, improvement or change to current business workflow, process etc. 
-->

### Target timeline

<!-- Format mm/dd/yyyy.
Date needed in production (e.g. federally mandated date)
-->

## Business Requirements

Business requirements describe the needs of the business to support processes, policy, business rules, etc in relation to the change requested.Complete each section below and discuss among all impacted program staff in advance of submission to ensure review of all program and end-to-end impact.
If a section does not apply, please input N/A. 

### Data Collection

<!-- new screen, new fields, new field values, etc. -->

### EDBC

<!-- Does this impact eligibility determination? What information for this change requires consideration? -->

### Customer Portal (AIE, AFB, RMC, RMB)

<!-- Describe how changes will impact the customer, specify considerations needed to be addressed, etc. -->

### Worker Portal 

<!-- Describe how changes will impact the worker’s system interaction, does it change workflow, specify any considerations that to be addressed, etc. -->

### Provider Portal

<!-- Describe how changes will impact the user’s system interaction and / or their needs, specify any considerations that need to be addressed, etc. -->

### Tasks

<!-- does this require creation of a new task, possible changes to a current task, etc.) -->

### Interfaces

<!-- Describe any impact to existing system interface…new or changing of data, is this a new interface to be considered? -->

### Notices

<!-- Describe any notice changes expected: Will it impact more than 1 agency, is this a format change, text change, etc. -->

### Reports

<!-- Does this impact existing reports resulting in changes, deletions, or possibly the creation of new report? -->

### BRS (Eligibility Logic Changes)

<!-- Describe the specific eligibility rule changes, specify business rules sheets, etc. -->

### Individuals to include in the CR Joint Application Requirements (JAR) and Joint-Application Design (JAD) Sessions

<!-- Use the @username mention notation to tag users here -->

<!-- quick actions go here.  See https://docs.gitlab.com/ee/user/project/quick_actions.html
example: /label ~enhancement
-->

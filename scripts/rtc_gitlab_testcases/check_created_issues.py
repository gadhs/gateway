import xmltodict
import requests
import os
import csv
import json
import datetime
import collections

GITLAB_BASE_URL="https://gitlab.com/api/v4/projects/42712479" # configuration-project: https://gitlab.com/gadhs/ogg/application/configuration-project
ISSUES_PATH="/issues"
RELEASE_LABEL="Release 35.0"
CREATED_AFTER="2023-05-17T08:00:00Z"

def api_call(http_verb, full_url=None, base_url=None, api_endpoint=None, headers=None, request_body=None, params={}):

    if full_url is None:
        url = base_url+api_endpoint
        payload=request_body
        params['private_token']=os.environ['GITLAB_TOKEN']
        files=[]
        headers = {}
        response = requests.request(http_verb, url, headers=headers, data=payload, files=files, params=params)
    else:
        response = requests.request(http_verb, full_url)

    full_response = response.json()

    while 'next' in response.links and response.links['next']['url'] is not None:
        response = requests.request(http_verb, response.links['next']['url'])
        full_response=merge_list(full_response, response.json())

    return full_response

def merge_dict(dict1, dict2):
    res = {**dict1, **dict2}
    return res

def merge_list(list1, list2):
    res = list1+list2
    return res

def fetch_created_issues():

    ## Fetch all existing issues and hold in memory to ensure duplicate issues are not created.
    # resp = api_call('GET', GITLAB_BASE_URL, ISSUES_PATH, params={'order_by':'title', 'state':'opened', 'labels':'Release 35.0', 'author_username': 'rohit.mathew'})
    resp = api_call('GET', base_url=GITLAB_BASE_URL, api_endpoint=ISSUES_PATH, params={'order_by':'title', 'state':'opened', 'labels':RELEASE_LABEL, 'author_username': 'rohit.mathew', 'per_page':100, 'created_after': CREATED_AFTER})
    existing_issues=[]

    for issue in resp:
        existing_issues.append(issue['title'])

    print("{} existing issues.".format(len(existing_issues)))
    print("duplicated issues: {}".format([item for item, count in collections.Counter(existing_issues).items() if count > 1]))
    existing_issues=[*set(existing_issues)]
    print("{} de-duped existing issues.".format(len(existing_issues)))

    print('Found {} existing issues'.format(len(existing_issues)))
    return existing_issues

def fetch_provided_issues(filename, provided_issues):

    file = open(filename, "r")
    reader = csv.reader(file, delimiter=',')
    for row in reader:
        if '-' in row[2]:
            provided_issues.append(row[2])

    return provided_issues

def compute_diff(provided_list, created_list):
    temp = []
    for element in provided_list:
        if element not in created_list:
            temp.append(element)

    return temp

def main():

    provided_issues=[]
    for csv_file in ["data_sources/pathways_scripts_phase3_csv.csv"]:
        provided_issues=fetch_provided_issues(csv_file, provided_issues)

    created_issues=fetch_created_issues()

    print("Issue Diff: \n")
    print(compute_diff(provided_issues, created_issues))

if __name__ == "__main__":
    main()


print("Coverting XML test scripts to Gitlab issues")
print("\n")

import xmltodict
import requests
import os
import csv
import json
import datetime
import collections

## 1. Convert XML to JSON
## 2. Iterate over 'entry' list
## 3. For each entry: iterate over the steps and extract relevant fields and construct a markdown formatted string:
## Step <name>
## Description: <description>
## Expected Result: <expectedResults>
## Construct payload to Issues API and fire


# DATA_SOURCES=["data_sources/testscripts_page_35.xml","data_sources/testscripts_page_37.xml","data_sources/testscripts_page_36.xml" ]
DATA_SOURCE_DIR="/home/rohit/Virtus/GATEWAY/gateway/scripts/rtc_gitlab_testcases/data_sources/ies_testscripts/"
ISSUES_CSV="data_sources/pathways_scripts_phase3_csv.csv"
UAT_METADATA_DIR="/home/rohit/Virtus/GATEWAY/gateway/scripts/rtc_gitlab_testcases/data_sources/testcase_page_40/"
# GITLAB_BASE_URL="https://gitlab.com/api/v4/projects/38315607" GATEWAY: https://gitlab.com/gadhs/ogg/gateway
# GITLAB_BASE_URL="https://gitlab.com/api/v4/projects/41808786" # customer-portal-test: https://gitlab.com/gadhs/ogg/application/customer-portal-test
GITLAB_BASE_URL="https://gitlab.com/api/v4/projects/42712479" # configuration-project: https://gitlab.com/gadhs/ogg/application/configuration-project
ISSUES_PATH="/issues"

RELEASE_LABEL="Release 35.0"

created_issues=[]
created_issue_ids=[]
errored_issues=[]
skipped_issues=[]
uat_scripts_found=0
uat_scripts_processed=0
issue_titles_to_create=[]
approved_track_labels=["Customer Portal", "Worker Portal"]
approved_program_labels=["ADA", "All", "CC", "FS", "GRG", "MA", "RF", "TANF", "TF", "WIC", "Pathways", "Medicaid"]


def api_call(http_verb, full_url=None, base_url=None, api_endpoint=None, headers=None, request_body=None, params={}):

    if full_url is None:
        url = base_url+api_endpoint
        payload=request_body
        params['private_token']=os.environ['GITLAB_TOKEN']
        files=[]
        headers = {}
        response = requests.request(http_verb, url, headers=headers, data=payload, files=files, params=params)
    else:
        response = requests.request(http_verb, full_url)

    full_response = response.json()

    # handle pagination
    while 'next' in response.links and response.links['next']['url'] is not None:
        response = requests.request(http_verb, response.links['next']['url'])
        full_response=merge_list(full_response, response.json())

    return full_response

def merge_dict(dict1, dict2):
    res = {**dict1, **dict2}
    return res

def merge_list(list1, list2):
    res = list1+list2
    return res

def create_gitlab_issue(request_body):
    print("Creating gitlab issue")

    try:
        resp = api_call('POST', base_url=GITLAB_BASE_URL, api_endpoint=ISSUES_PATH, request_body=request_body)
    except requests.exceptions.RequestException as e:
        print("###### Unable to create issue {}. Skipping. #####".format(request_body['title']))
        global errored_issues
        errored_issues.append(request_body['title'])
        return

    print("###### Created issue {}. #####".format(request_body['title']))
    global created_issue_ids
    global created_issues
    created_issue_ids.append(str(resp['iid']))
    created_issues.append(request_body['title'])

def update_gitlab_issue(issue):
    print("Updating gitlab issue")

def process_entry(entry, existing_issues, uat_issues):

    ## Create the issue with all fields except description
    title=entry['title']['#text']

    # If specific issue titles are provided use those, otherwise only create issues provided in CSV
    if len(issue_titles_to_create) > 0:
        if title not in issue_titles_to_create:
            return
    else:
        if title not in uat_issues.keys():
            return

    print(f'{title} found in uat scripts')
    global uat_scripts_found
    uat_scripts_found+=1

    global uat_scripts_processed
    if uat_scripts_processed == 100:
        return
    uat_scripts_processed+=1
    print(json.dumps(entry))


    request_body={}
    request_body['title']="{}".format(title)
    request_body['issue_type']="test_case"

    if title in existing_issues:
        print("###### {} already exists. Skipping. ######".format(title))
        global skipped_issues
        skipped_issues.append(request_body['title'])
        return

    labels="{}, TOA Pathways, CR Number::Pathways, ".format(RELEASE_LABEL)

    test_data=uat_issues[title]

    description=""
    metadata_str=""

    metadata_str+="Test Case: {}<br>".format(title)
    metadata_str+="<br>"
    metadata_str+="Summary: {}<br>".format(test_data['metadata']['summary']['#text'])
    metadata_str+="<br>"

    metadata_str+="Test Lead: @{}<br>".format(test_data['metadata']['content']['ns2:testcase']['ns4:creator']['@name'])
    metadata_str+="<br>"

    if 'ns2:category' in test_data['metadata']['content']['ns2:testcase']:
        requirements=[]
        for category in test_data['metadata']['content']['ns2:testcase']['ns2:category']:
            if category['@term'] == "RQ. ID":
                requirements.append(category['@value'])
                continue
            else:
                if category['@term'] == "Program":
                    if category['@value']=='FS|MA|TANF' or category['@value']=='All':
                        labels+="{} {},".format(category['@term'], category['@value'])
                    else:
                        program_labels=""
                        program_label_arr=category['@value'].split('/')
                        for program in program_label_arr:
                            if program not in approved_program_labels: continue
                            program_labels+="{} {},".format(category['@term'], program)
                        labels+=program_labels
                if category['@term'] == "Track":
                    value = category['@value']
                    for track in approved_track_labels:
                        if track in value:
                            labels+="{}::{},".format(category['@term'], track)
                            value=(value.replace(track, "")).strip()
                    for track in value.split(" "):
                            if track=="/" or track=="": continue
                            labels+="{}::{},".format(category['@term'], track)
                metadata_str+="{}: {}<br>".format(category['@term'], category['@value'])
                metadata_str+="<br>"

    description+=metadata_str

    req_string="Requirement ID(s): \n"
    for req in requirements:
        req_string+="* {} \n".format(req)

    description+=req_string

    if 'ns8:name' in entry['content']['ns2:testscript']['ns2:steps']['ns8:step']:
        print("{} only has 1 step".format(title))
        step_str=""
        try:
            step=entry['content']['ns2:testscript']['ns2:steps']['ns8:step']
            step_str+="{}. {}\n".format(step['ns8:name'], step['ns8:description']['div']['#text'])
            step_str+="\n"
            step_str+="\t - Type: {}\n".format(step['@type'])
            step_str+="\t - Expected Result:\n"
            step_str+="\t\t - [ ] {}\n".format(step['ns8:expectedResult']['div']['#text'])
            step_str+="\n"
        except:
            pass
        description+=step_str
    else:
        for step in entry['content']['ns2:testscript']['ns2:steps']['ns8:step']:
            step_str=""
            try:
                step_str+="{}. {}\n".format(step['ns8:name'], step['ns8:description']['div']['#text'])
                step_str+="\n"
                step_str+="\t - Type: {}\n".format(step['@type'])
                step_str+="\t - Expected Result:\n"
                step_str+="\t\t - [ ] {}\n".format(step['ns8:expectedResult']['div']['#text'])
                step_str+="\n"
            except:
                pass
            description+=step_str

    request_body['description']=description
    request_body['labels']=labels
    create_gitlab_issue(request_body)

def fetch_existing_issues():

    ## TODO: Make params configurable.
    ## Fetch all existing issues and hold in memory to ensure duplicate issues are not created.
    resp = api_call('GET', base_url=GITLAB_BASE_URL, api_endpoint=ISSUES_PATH, params={'order_by':'title', 'state':'opened', 'labels':RELEASE_LABEL, 'author_username': 'rohit.mathew', 'per_page':100})
    existing_issues=[]

    for issue in resp:
        existing_issues.append(issue['title'])

    print("{} existing issues.".format(len(existing_issues)))
    print("duplicated issues: {}".format([item for item, count in collections.Counter(existing_issues).items() if count > 1]))
    existing_issues=[*set(existing_issues)]
    print("{} unique existing issues.".format(len(existing_issues)))

    return existing_issues

def write_issues(issues, filename):
    for issue in issues:
        filename.write(str(issue) + '\n')

def print_issues(issues):
    for issue in issues:
        print("{}\n".format(issue))

def build_uat_dict():

    items={}

    file = open(ISSUES_CSV, "r")
    reader = csv.reader(file, delimiter=',')
    for row in reader:
        entry={}
        entry['Test_Plan']=row[0]
        entry['Test_Plan_Id']=row[1]
        # entry['Program']=row[3]
        items[row[2]]=entry

    for file in os.listdir(UAT_METADATA_DIR):
         filename = os.fsdecode(file)
         if filename.startswith("testcase_page_") and filename.endswith(".xml"):
            with open(os.path.join(UAT_METADATA_DIR, filename), "rb") as file:
                 document = xmltodict.parse(file)

            for entry in document['feed']['entry']:
                if entry['title']['#text'] in items.keys():
                    items[entry['title']['#text']]['metadata']=entry
            continue
         else:
             continue

    return items

def write_log_file():

    run_log = open('run_log_{}.txt'.format(datetime.datetime.now()), 'w')

    global created_issues
    created_issues.insert(0, "##### Created Issues:")
    write_issues(created_issues, run_log)

    global errored_issues
    errored_issues.insert(0, "##### Did not create the following errored issues:")
    write_issues(errored_issues, run_log)

    global skipped_issues
    skipped_issues.insert(0, "##### Did not create the following duplicate issues:")
    write_issues(skipped_issues, run_log)

    run_log.write(str("Created Issue Ids:") + '\n')
    run_log.write("[")
    for issue_id in created_issue_ids:
        run_log.write(issue_id)
        run_log.write(",")
    run_log.write("]")

def main():

    uat_issues=build_uat_dict()
    output_log = open('uat_metadata.json', 'w')
    output_log.write(json.dumps(uat_issues))

    output_log = open('uat_metadata_keys.json', 'w')
    output_log.write(str(uat_issues.keys()))

    existing_issues=fetch_existing_issues()

    for data_file in os.listdir(DATA_SOURCE_DIR):
         filename = os.fsdecode(data_file)
         if filename.startswith("testscripts_page_") and filename.endswith(".xml"):
            with open(os.path.join(DATA_SOURCE_DIR, filename), "rb") as file:
                 document = xmltodict.parse(file)
            for entry in document['feed']['entry']:
                try:
                    process_entry(entry, existing_issues, uat_issues)
                except Exception:
                    pass

    print(uat_scripts_found)
    print(uat_scripts_processed)
    write_log_file()

if __name__ == "__main__":
    main()

